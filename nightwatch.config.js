module.exports = {
  selenium: {
    cli_args: {
      'webdriver.gecko.driver': require('geckodriver').path, // eslint-disable-line global-require, import/no-extraneous-dependencies
    },
  },
  test_settings: {
    firefox: {
      desiredCapabilities: {
        browserName: 'firefox',
        javascriptEnabled: true,
        acceptSslCerts: true,
      },
    },
  },
};
