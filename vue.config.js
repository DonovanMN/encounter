const path = require('path');

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/encounter/' : '/',
  pwa: {
    name: 'Encounter Assist',
    themeColor: '#546E7A',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    workboxOptions: {
      importScripts: ['/encounter/sw-skip-waiting.js'],
    },
  },
  chainWebpack: (config) => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => ({
        ...options,
        hotReload: false,
      }));
    config.resolve.alias
      .set('#', path.join(__dirname, 'tests'));
  },
};
