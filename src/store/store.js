import Vue from 'vue';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import VuexPersistence from 'vuex-persist';
import localForage from 'localforage';
import db from '@/config/database';

import reload from './modules/reload';
import peer from './modules/peer';
import initiative from './modules/initiative';

Vue.use(Vuex);

export { db };

export const persist = new VuexPersistence({
  asyncStorage: true,
  storage: localForage,
  modules: [
    'peer',
    'entities',
  ],
});

export default new Vuex.Store({
  modules: {
    reload,
    peer,
    initiative,
  },
  plugins: [
    VuexORM.install(db),
    persist.plugin,
  ],
});
