/* eslint no-param-reassign: ["error", { "props": false }] */

export default {
  namespaced: true,

  state: {
    registration: null,
    prompt: false,
  },

  mutations: {
    showReloadPrompt(state) {
      state.prompt = true;
    },
    hideReloadPrompt(state) {
      state.prompt = false;
    },
    setRegistration(state, registration) {
      state.registration = registration;
    },
  },

  actions: {
    prompt({ commit }, registration) {
      commit('setRegistration', registration);
      commit('showReloadPrompt');
    },
    triggerReload({ commit, state }) {
      commit('hideReloadPrompt');
      if (state.registration && state.registration.waiting) {
        state.registration.waiting.postMessage('skipWaiting');
      }
    },
  },
};
