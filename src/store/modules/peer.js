/* eslint no-param-reassign: ["error", { "props": false }] */

export default {
  namespaced: true,

  state: {
    peerId: undefined,
    encounterRunId: undefined,
  },

  mutations: {
    setPeerId(state, peerId) {
      state.peerId = peerId;
    },
    runEncounter(state, encounterRunId) {
      state.encounterRunId = encounterRunId;
    },
  },
};
