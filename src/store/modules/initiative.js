/* eslint no-param-reassign: ["error", { "props": false }] */
import EncounterRunFoe from '../../models/EncounterRunFoe';
import Member from '../../models/Member';

export default {
  namespaced: true,

  state: {
    connection: undefined,
    combatants: [],
    turn: null,
    nextTurn: null,
  },

  mutations: {
    setConnection(state, connection) {
      state.connection = connection;
    },

    updateCombatants(state, combatants) {
      state.combatants = combatants;
    },

    updateTurn(state, { turn, nextTurn }) {
      state.turn = turn;
      state.nextTurn = nextTurn;
    },
  },

  getters: {
    combatants: state => (
      state.combatants.map((combatant) => {
        if (combatant.foe) {
          return new EncounterRunFoe(combatant);
        }
        return new Member(combatant);
      })
    ),
  },
};
