import Tag from '@/models/Tag';

export default function updateTags(instance, tags) {
  const {
    tags: {
      pivot: PivotModel,
      foreignPivotKey,
    },
  } = instance.constructor.fields();

  const newTags = Tag.normalize(tags || []);
  const oldTags = instance.tags;

  const deletedTagValues = oldTags.map(tag => tag.value).filter(oldTagValue => (
    !newTags.some(newTag => newTag.value === oldTagValue)
  ));

  if (deletedTagValues.length) {
    PivotModel.delete(pivot => (
      pivot[foreignPivotKey] === instance.id && deletedTagValues.includes(pivot.tag_value)
    ));
  }

  const addedTags = newTags.filter((newTag) => {
    const newTagValue = newTag.value;
    return !oldTags.some(oldTag => oldTag.value === newTagValue);
  });

  if (addedTags.length) {
    Tag.insertOrUpdate({
      data: addedTags,
    });

    PivotModel.insert({
      data: addedTags.map(tag => ({ [foreignPivotKey]: instance.id, tag_value: tag.value })),
    });
  }
}
