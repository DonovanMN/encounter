export const initiativeMax = 50;
export const initiativeMin = -9;
export default [
  value => !value || !Number.isNaN(+value) ||
    'Initiative must be a number',
  value => !value || value <= initiativeMax ||
    `Initiative must be ${initiativeMax} or less`,
  value => !value || value >= initiativeMin ||
    `Initiative must be ${initiativeMin} or more`,
];
