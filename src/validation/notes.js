export const notesLimit = 1000;
export default [
  value => !value || value.length <= notesLimit ||
    `Notes must be less than ${notesLimit} characters`,
];
