export const nameLimit = 50;
export default [
  value => !!value || 'Name is required',
  value => !value || value.length <= nameLimit ||
    `Name must be less than ${nameLimit} characters`,
];
