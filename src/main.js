import '@babel/polyfill';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './config/router';
import store from './store/store';
import vuetify from './plugins/vuetify';
import App from './App.vue';
import './registerServiceWorker';

Vue.config.productionTip = false;

Vue.use(VueRouter);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
}).$mount('#app');
