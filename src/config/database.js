import VuexORM from '@vuex-orm/core';

import Member from '@/models/Member';
import Tag from '@/models/Tag';
import Foe from '@/models/Foe';
import FoeTag from '@/models/FoeTag';
import Encounter from '@/models/Encounter';
import EncounterTag from '@/models/EncounterTag';
import EncounterFoe from '@/models/EncounterFoe';
import EncounterRun from '@/models/EncounterRun';
import EncounterRunFoe from '@/models/EncounterRunFoe';

const db = new VuexORM.Database();

db.register(Member);
db.register(Tag);
db.register(Foe);
db.register(FoeTag);
db.register(Encounter);
db.register(EncounterTag);
db.register(EncounterFoe);
db.register(EncounterRun);
db.register(EncounterRunFoe);

export default db;
