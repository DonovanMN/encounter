import VueRouter from 'vue-router';

export default new VueRouter({
  routes: [
    {
      path: '/',
      redirect: { name: 'encounters' },
    },
    {
      path: '/encounters',
      name: 'encounters',
      component: () => import(/* webpackChunkName: "encounters" */ '@/views/Encounters.vue'),
    },
    {
      path: '/foes',
      name: 'foes',
      component: () => import(/* webpackChunkName: "foes" */ '@/views/Foes.vue'),
    },
    {
      path: '/party',
      name: 'party',
      component: () => import(/* webpackChunkName: "party" */ '@/views/Party.vue'),
    },
    {
      path: '/encounter/:id',
      name: 'encounter',
      component: () => import(/* webpackChunkName: "encounter" */ '@/views/Encounter.vue'),
      props: true,
    },
    {
      path: '/initiative/:hostId',
      name: 'initiative',
      component: () => import(/* webpackChunkName: "initiative" */ '@/views/Initiative.vue'),
      props: true,
      meta: {
        playerView: true,
      },
    },
    {
      path: '/import_export/',
      name: 'import_export',
      component: () => import(/* webpackChunkName: "import_export" */ '@/views/ImportExport.vue'),
      props: true,
    },
  ],
});
