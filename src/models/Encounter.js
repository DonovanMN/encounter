import { Model } from '@vuex-orm/core';
import Tag from './Tag';
import EncounterTag from './EncounterTag';
import Foe from './Foe';
import EncounterFoe from './EncounterFoe';
import EncounterRun from './EncounterRun';
import Member from './Member';

export default class Encounter extends Model {
  static entity = 'encounters';

  static fields() {
    return {
      id: this.increment(),
      name: this.string(''),
      order: this.number(1),
      tags: this.belongsToMany(Tag, EncounterTag, 'encounter_id', 'tag_value'),
      foes: this.belongsToMany(Foe, EncounterFoe, 'encounter_id', 'foe_id'),
      notes: this.string(''),
      run: this.hasOne(EncounterRun, 'encounter_id'),
    };
  }

  get enemies() {
    return this.foes && this.foes.filter(foe => !this.isAlly(foe));
  }

  get enemyTotal() {
    const { enemies } = this;
    return !enemies ? 0 : enemies.reduce((total, foe) => (
      total + this.foeCount(foe)
    ), 0);
  }

  addFoe(foe) {
    EncounterFoe.insert({
      data: {
        encounter_id: this.id,
        foe_id: foe.id,
      },
    });
  }

  removeFoe(foe) {
    EncounterFoe.delete(`${this.id}_${foe.id}`);
    this.updateRun();
  }

  incrementFoe(foe) {
    const id = `${this.id}_${foe.id}`;
    const ef = EncounterFoe.find(id);
    if (ef) {
      this.setFoeCount(foe, ef.count + 1);
    }
  }

  decrementFoe(foe) {
    const id = `${this.id}_${foe.id}`;
    const ef = EncounterFoe.find(id);
    if (ef) {
      this.setFoeCount(foe, ef.count - 1);
    }
  }

  setFoeCount(foe, count = 1) {
    return EncounterFoe.update({
      data: {
        encounter_id: this.id,
        foe_id: foe.id,
        count,
      },
    });
  }

  foeCount(foe) {
    const ef = EncounterFoe.find(`${this.id}_${foe.id}`);
    return ef ? ef.count : 0;
  }

  toggleAlly(foe) {
    const id = `${this.id}_${foe.id}`;
    const ef = EncounterFoe.find(id);
    if (ef) {
      EncounterFoe.update({
        data: {
          ...ef,
          ally: !ef.ally,
        },
      });
    }
  }

  isAlly(foe) {
    if (!foe) {
      return false;
    }
    const ef = EncounterFoe.find(`${this.id}_${foe.id}`);
    return ef && ef.ally;
  }

  get xp() {
    const { enemies } = this;
    return !enemies ? 0 : enemies.reduce((total, foe) => (
      total + (this.foeCount(foe) * foe.xp)
    ), 0);
  }

  get xpAdjusted() {
    const { xp, enemyTotal } = this;
    if (!xp || !enemyTotal) {
      return 0;
    }

    let index = 1;
    if (enemyTotal >= 15) {
      index = 6;
    } else if (enemyTotal >= 11) {
      index = 5;
    } else if (enemyTotal >= 7) {
      index = 4;
    } else if (enemyTotal >= 3) {
      index = 3;
    } else if (enemyTotal === 2) {
      index = 2;
    }

    const partySize = Member.query().where('present', true).count();
    if (partySize >= 6) {
      index -= 1;
    } else if (partySize < 3) {
      index += 1;
    }

    const multipliers = [0.5, 1, 1.5, 2, 2.5, 3, 4, 5];
    return xp * multipliers[index];
  }

  get difficulty() {
    const {
      easy,
      medium,
      hard,
      deadly,
    } = Member.partyThesholds();
    const { xpAdjusted } = this;

    let difficulty = 'trivial';
    if (xpAdjusted >= deadly) {
      difficulty = 'deadly';
    } else if (xpAdjusted >= hard) {
      difficulty = 'hard';
    } else if (xpAdjusted >= medium) {
      difficulty = 'medium';
    } else if (xpAdjusted >= easy) {
      difficulty = 'easy';
    }
    return difficulty;
  }

  updateRun() {
    const { run } = this;
    if (!run) {
      EncounterRun.insert({
        data: {
          encounter_id: this.id,
        },
      }).then(({ encounterRuns }) => {
        encounterRuns[0].updateFoes(this);
      });
    } else {
      run.updateFoes(this);
    }
  }
}
