import { Model } from '@vuex-orm/core';
import EncounterRunFoe from './EncounterRunFoe';
import Member from './Member';

function skipTurn(combatant, prevLiving) {
  return combatant.isDead() || (
    combatant.foe && prevLiving && combatant.isGroupedWith(prevLiving)
  );
}

export default class EncounterRun extends Model {
  static entity = 'encounterRuns';

  static fields() {
    return {
      id: this.increment(),
      encounter_id: this.attr(null),
      foes: this.hasMany(EncounterRunFoe, 'encounter_run_id'),
      turn: this.number(null),
    };
  }

  get nextTurn() {
    let { turn } = this;
    if (turn === null || Number.isNaN(turn)) {
      turn = 0;
    } else {
      const combatants = this.combatants();
      let combatant = combatants[turn];
      let prevLiving;
      do {
        if (!combatant.isDead()) {
          prevLiving = combatant;
        }
        turn = (turn + 1) % this.numCombatants();
        combatant = combatants[turn];
      } while (skipTurn(combatant, prevLiving));
    }
    return turn;
  }

  updateNextTurn(turn = this.nextTurn) {
    this.$update({ turn });
  }

  foeCount(foe, living = false) {
    let query = EncounterRunFoe.query()
      .where('foe_id', foe.id)
      .where('encounter_run_id', this.id);
    if (living) {
      query = query.where('hitPoints', hp => hp > 0);
    }
    return query.count();
  }

  combatants() {
    return [
      ...EncounterRunFoe.query()
        .withAllRecursive()
        .where('encounter_run_id', this.id)
        .get(),
      ...Member.party(),
    ].sort((a, b) => {
      // Order by initative, descending
      const initA = a.initiative;
      const initB = b.initiative;
      if (initA < initB) {
        return 1;
      }
      if (initA > initB) {
        return -1;
      }

      // Then Foes at the bottom
      if (a.foe && !b.foe) {
        return 1;
      }
      if (!a.foe && b.foe) {
        return -1;
      }

      // Then by init mod, descending
      const initModA = a.initMod;
      const initModB = b.initMod;
      if (initModA < initModB) {
        return 1;
      }
      if (initModA > initModB) {
        return -1;
      }

      // Then order by name, ascending
      const nameA = a.name.toLowerCase();
      const nameB = b.name.toLowerCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
  }

  numCombatants() {
    return EncounterRunFoe.query().where('encounter_run_id', this.id).count() +
      Member.partySize();
  }

  updateFoes(encounter) {
    const { foes = [] } = this;
    if (!foes.length) {
      EncounterRunFoe.insert({
        data: encounter.foes.flatMap((foe) => {
          const count = encounter.foeCount(foe);
          return Array(count).fill(null).map(() => foe.toRunFoe(this.id));
        }),
      });
    } else {
      encounter.foes.forEach((foe) => {
        const foeCount = encounter.foeCount(foe);
        const erfCount = this.foeCount(foe);
        if (foeCount > erfCount) {
          EncounterRunFoe.insert({
            data: foe.toRunFoe(this.id),
          });
        } else if (foeCount < erfCount) {
          EncounterRunFoe.query()
            .where('foe_id', foe.id)
            .where('encounter_run_id', this.id)
            .orderBy('id', 'desc')
            .limit(erfCount - foeCount)
            .get()
            .forEach(erf => erf.$delete());
        }
      });
    }
  }
}
