import { Model } from '@vuex-orm/core';

export default class Creature extends Model {
  static fields() {
    return {
      initMod: this.number(0),
    };
  }

  static roll(min, max) {
    return Math.floor((Math.random() * ((max - min) + 1)) + min);
  }

  static d20Roll() {
    return Creature.roll(1, 20);
  }

  constructor(record, trackHitPoints = false) {
    super(record);
    this.trackHitPoints = trackHitPoints;
  }

  get initModDisplay() {
    const { initMod } = this;
    return initMod >= 0 ? `+${initMod}` : `${initMod}`;
  }

  isDead() {
    return this.trackHitPoints && this.hitPoints <= 0;
  }

  rollInitiative() {
    return Creature.d20Roll() + this.initMod;
  }
}
