import { Model } from '@vuex-orm/core';

export default class EncounterTag extends Model {
  static entity = 'encounterTag';

  static primaryKey = ['encounter_id', 'tag_value'];

  static fields() {
    return {
      encounter_id: this.attr(null),
      tag_value: this.string(''),
    };
  }
}
