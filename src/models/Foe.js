import Creature from './Creature';
import Tag from './Tag';
import FoeTag from './FoeTag';

export default class Foe extends Creature {
  static entity = 'foes';

  static fields() {
    return {
      id: this.increment(),
      ...Creature.fields(),
      name: this.string(''),
      cr: this.number(0),
      maxHitPoints: this.number(10),
      armorClass: this.number(10),
      tags: this.belongsToMany(Tag, FoeTag, 'foe_id', 'tag_value'),
      notes: this.string(''),
    };
  }

  constructor(record) {
    super(record, true);
  }

  static crToText(cr) {
    if (cr >= 1 || cr === 0) {
      return `${cr}`;
    }
    let denominator;
    for (denominator = 1; (cr * denominator) % 1 !== 0; denominator += 1);
    const numerator = cr * denominator;
    return `${numerator}/${denominator}`;
  }

  get crText() {
    return Foe.crToText(this.cr);
  }

  get xp() {
    const { cr } = this;
    if (cr <= 1) {
      return cr * 200;
    }
    return [
      450, 700, 1100, 1800, 2300, 2900, 3900, 5000, 5900, 7200,
      8400, 10000, 11500, 13000, 15000, 18000, 20000, 22000, 25000, 33000,
      41000, 50000, 62000, 75000, 90000, 105000, 120000, 135000, 155000,
    ][cr - 2];
  }

  toRunFoe(runId) {
    return {
      encounter_run_id: runId,
      foe_id: this.id,
      hitPoints: this.maxHitPoints,
    };
  }
}
