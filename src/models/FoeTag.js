import { Model } from '@vuex-orm/core';

export default class FoeTag extends Model {
  static entity = 'foeTag';

  static primaryKey = ['foe_id', 'tag_value'];

  static fields() {
    return {
      foe_id: this.attr(null),
      tag_value: this.string(''),
    };
  }
}
