import { Model } from '@vuex-orm/core';

export default class Tag extends Model {
  static entity = 'tags';

  static primaryKey = 'value';

  static fields() {
    return {
      value: this.string(''),
    };
  }

  static normalize(tag) {
    const tags = Array.isArray(tag) ? tag : [tag];
    return tags.map((objOrString) => {
      if (typeof objOrString === 'string') {
        return { value: objOrString.toLowerCase() };
      }
      return objOrString;
    });
  }

  get text() {
    const { value } = this;
    if (!value) {
      return '';
    }
    if (value.length < 3) {
      return value.toUpperCase();
    }
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
}
