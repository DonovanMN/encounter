import Creature from './Creature';
import { initiativeMax, initiativeMin } from '../validation/initiative';

export default class Member extends Creature {
  static entity = 'members';

  static fields() {
    return {
      id: this.increment(),
      ...Creature.fields(),
      name: this.string(''),
      level: this.number(1),
      present: this.boolean(true),
      initiative: this.number(0),
      notes: this.string(''),
    };
  }

  static updateInitiative(id, initiative) {
    if (initiative <= initiativeMax && initiative >= initiativeMin) {
      Member.update({
        where: id,
        data: {
          initiative,
        },
      });
    }
  }

  static party() {
    return Member.query().where('present', true).get();
  }

  static partySize() {
    return Member.query().where('present', true).count();
  }

  static partyThesholds() {
    const party = Member.party();
    const partyThesholds = {};
    Object.keys(Member.thresholds).forEach((difficulty) => {
      const difficultyThresholds = Member.thresholds[difficulty];
      const total = party.reduce((sum, member) => (
        sum + difficultyThresholds[member.level - 1]
      ), 0);
      partyThesholds[difficulty] = total;
    });
    return partyThesholds;
  }

  static partyAdventuringDay() {
    const party = Member.party();
    return party.reduce((sum, member) => (
      sum + Member.adventuringDay[member.level - 1]
    ), 0);
  }

  static partyAdventuringDayThird() {
    return Math.floor(Member.partyAdventuringDay() / 3);
  }

  static thresholds = {
    easy: [
      25, 50, 75, 125, 250, 300, 350, 450, 550, 600,
      800, 1000, 1100, 1250, 1400, 1600, 2000, 2100, 2400, 2800,
    ],
    medium: [
      50, 100, 150, 250, 500, 600, 750, 900, 1100, 1200,
      1600, 2000, 2200, 2500, 2800, 3200, 3900, 4200, 4900, 5700,
    ],
    hard: [
      75, 150, 225, 375, 750, 900, 1100, 1400, 1600, 1900,
      2400, 3000, 3400, 3800, 4300, 4800, 5900, 6300, 7300, 8500,
    ],
    deadly: [
      100, 200, 400, 500, 1100, 1400, 1700, 2100, 2400, 2800,
      3600, 4500, 5100, 5700, 6400, 7200, 8800, 9500, 10900, 12700,
    ],
  };

  static adventuringDay = [
    300, 600, 1200, 1700, 3500, 4000, 5000, 6000, 7500, 9000,
    10500, 11500, 13500, 15000, 18000, 20000, 25000, 27000, 30000, 40000,
  ];

  get key() {
    return `${this.constructor.name}_${this.id}`;
  }
}
