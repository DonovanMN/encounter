import { Model } from '@vuex-orm/core';
import Foe from './Foe';

function findFoe(erf) {
  return erf.foe || Foe.find(erf.foe_id);
}

function getFoeProp(erf, propName) {
  return findFoe(erf)[propName];
}

function callFoeMethod(erf, methodName, args = []) {
  return getFoeProp(erf, methodName).apply(erf, args);
}

export default class EncounterRunFoe extends Model {
  static entity = 'encounterRunFoes';

  static fields() {
    return {
      id: this.increment(),
      encounter_run_id: this.attr(null),
      foe_id: this.attr(null),
      foe: this.belongsTo(Foe, 'foe_id'),
      hitPoints: this.number(null),
      initiative: this.number(null),
      notes: this.string(''),
      hidden: this.boolean(true),
    };
  }

  get key() {
    return `${this.constructor.name}_${this.id}`;
  }

  get trackHitPoints() {
    return getFoeProp(this, 'trackHitPoints');
  }

  get name() {
    return getFoeProp(this, 'name');
  }

  get maxHitPoints() {
    return getFoeProp(this, 'maxHitPoints');
  }

  get initMod() {
    return getFoeProp(this, 'initMod');
  }

  get initModDisplay() {
    return getFoeProp(this, 'initModDisplay');
  }

  get armorClass() {
    return getFoeProp(this, 'armorClass');
  }

  get crText() {
    return getFoeProp(this, 'crText');
  }

  isDead() {
    return callFoeMethod(this, 'isDead');
  }

  isGroupedWith(other) {
    return this.foe_id && other.foe_id && this.foe_id === other.foe_id &&
      this.initiative === other.initiative;
  }
}
