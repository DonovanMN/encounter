import { Model } from '@vuex-orm/core';

export default class EncounterFoe extends Model {
  static entity = 'encounterFoes';

  static primaryKey = ['encounter_id', 'foe_id'];

  static fields() {
    return {
      encounter_id: this.attr(null),
      foe_id: this.attr(null),
      count: this.number(1),
      ally: this.boolean(false),
    };
  }
}
