/* eslint-disable no-restricted-globals */

self.addEventListener('message', (event) => {
  if (!event.data) {
    return;
  }

  if (event.data === 'skipWaiting') {
    self.skipWaiting();
  }
});
