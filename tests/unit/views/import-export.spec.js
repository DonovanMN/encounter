import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import localForage from 'localforage';
import { saveAs } from 'file-saver';

import ImportExport from '@/views/ImportExport.vue';

jest.mock('localforage');
jest.mock('file-saver', () => ({
  _esModule: true,
  saveAs: jest.fn(),
}));

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuetify);

const readBlob = blob => (
  new Promise((resolve) => {
    const reader = new FileReader();
    reader.addEventListener('loadend', () => {
      resolve(reader.result);
    });
    reader.readAsText(blob);
  })
);

const entities = {
  foo: ['foo1', 'foo2'],
  bar: {
    one: 1,
    two: 2,
  },
};

const importedEntities = {
  foo: ['foo1', 'foo3', 'foo4'],
  bar: {
    one: 1,
    three: 3,
  },
  baz: 'quux',
};

describe('ImportExport.vue', () => {
  let wrapper;

  beforeEach(() => {
    const vuetify = new Vuetify();

    wrapper = mount(ImportExport, { localVue, vuetify });
  });

  describe('export', () => {
    let getItemPromise;

    beforeEach(() => {
      getItemPromise = Promise.resolve({
        entities,
      });
      localForage.getItem.mockReturnValue(getItemPromise);
    });

    it('saves entities to a timestamped file', async () => {
      wrapper.find('.import-export__export-data').trigger('click');
      expect(localForage.getItem).toBeCalledWith('vuex');
      await getItemPromise;
      expect(saveAs).toBeCalled();
      const [blob, filename] = saveAs.mock.calls[0];
      expect(filename).toMatch(/^encounter-assist_[0-9]{8}T[0-9]{6}.json$/);
      expect(blob).toBeInstanceOf(Blob);
      const json = await readBlob(blob);
      expect(JSON.parse(json)).toEqual(entities);
    });
  });

  describe('import', () => {
    let importDataButton;

    beforeEach(() => {
      importDataButton = wrapper.find('.import-export__import-data');
    });

    describe('button', () => {
      it('is disabled', () => {
        expect(importDataButton.attributes().disabled).toBe('disabled');
      });

      describe('when a file is selected', () => {
        beforeEach(() => {
          wrapper.setData({ fileSelected: true });
        });

        it('is enabled', () => {
          expect(importDataButton.attributes().disabled).toBeUndefined();
        });
      });
    });

    describe('importData method', () => {
      let setItemPromise;

      beforeEach(() => {
        // JSDOM Doesn't have DataTransfer class necessary to simulate
        // selecting a file in the input, so manipulate the $refs instead
        wrapper.vm.$refs.importFile = {
          files: [
            new Blob(
              [JSON.stringify(importedEntities, null, 2)],
              { type: 'application/json' },
            ),
          ],
        };
        wrapper.setData({ fileSelected: true });

        localForage.getItem.mockReturnValue(Promise.resolve({
          entities,
          otherData: true,
        }));

        let resolveSetItem;
        setItemPromise = new Promise((resolve) => {
          resolveSetItem = resolve;
        });
        localForage.setItem.mockImplementationOnce(() => {
          resolveSetItem();
          return setItemPromise;
        });

        Object.defineProperty(window.location, 'reload', {
          configurable: true,
        });
        window.location.reload = jest.fn();
      });

      it('overwrites entities with the imported data', async () => {
        expect(importDataButton.attributes().disabled).toBeUndefined();
        await importDataButton.trigger('click');
        expect(importDataButton.attributes().disabled).toBe('disabled');
        await setItemPromise;
        expect(localForage.setItem).toBeCalled();
        const [itemName, updatedData] = localForage.setItem.mock.calls[0];
        expect(itemName).toBe('vuex');
        expect(updatedData).toEqual({
          otherData: true,
          entities: importedEntities,
        });
        await setItemPromise;
        expect(window.location.reload).toBeCalled();
      });
    });
  });
});
