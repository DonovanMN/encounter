import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import cloneDeep from 'lodash.clonedeep';

import db from '@/config/database';
import Member from '@/models/Member';

import Party from '@/views/Party.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

describe('Party.vue', () => {
  let wrapper;
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep({
      plugins: [VuexORM.install(db)],
    }));

    const vuetify = new Vuetify();

    wrapper = mount(Party, { localVue, store, vuetify });
  });

  describe('add fab', () => {
    let addPersonBtn;

    beforeEach(() => {
      addPersonBtn = wrapper.find('.party .v-btn--fab');
    });

    it('adds party member on icon click', () => {
      expect(Member.all().length).toBe(0);
      addPersonBtn.trigger('click');
      expect(Member.all().length).toBe(1);
    });
  });

  describe('member panel', () => {
    let wolfram;
    let expansionPanel;

    beforeEach(async () => {
      const entities = await Member.create({ data: { name: 'Wolfram' } });
      [wolfram] = entities[Member.entity];
      expansionPanel = wrapper.find('.v-expansion-panel');
      await expansionPanel.find('.v-expansion-panel-header').trigger('click');
      await Vue.nextTick();
      expect(expansionPanel.classes()).toContain('v-expansion-panel--active');
    });

    it('can update the name', () => {
      const editNameField = expansionPanel.find('.edit-member .v-text-field');
      expect(wolfram.name).toBe('Wolfram');
      editNameField.vm.$emit('change', 'Wolfra');
      wolfram = Member.find(wolfram.id);
      expect(wolfram.name).toBe('Wolfra');
    });
  });
});
