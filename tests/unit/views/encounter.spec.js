import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import cloneDeep from 'lodash.clonedeep';

import db from '@/config/database';
import Member from '@/models/Member';
import Foe from '@/models/Foe';
import Encounter from '@/models/Encounter';
import EncounterRun from '@/models/EncounterRun';
import peer from '@/store/modules/peer';

import MEMBER_FIXTURES from '#/fixtures/member.fixtures';
import FOE_FIXTURES from '#/fixtures/foe.fixtures';
import ENCOUNTER_FIXTURES from '#/fixtures/encounter.fixtures';

import EncounterView from '@/views/Encounter.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

const FIXTURES = {
  ...MEMBER_FIXTURES,
  ...FOE_FIXTURES,
  ...ENCOUNTER_FIXTURES,
};

describe('Encounter.vue', () => {
  let wrapper;
  let store;
  let encounter;
  let bandit;

  beforeEach(async () => {
    store = new Vuex.Store(cloneDeep({
      modules: { peer },
      plugins: [VuexORM.install(db)],
    }));

    const vuetify = new Vuetify();

    await Member.create({ data: [FIXTURES.wolfram, FIXTURES.renauld] });

    wrapper = mount(EncounterView, {
      localVue,
      store,
      vuetify,
      stubs: ['router-link'],
      propsData: {
        id: `${FIXTURES.brawl.id}`,
      },
    });

    const entities = await Encounter.create({ data: FIXTURES.brawl });
    [encounter] = entities[Encounter.entity];
    bandit = entities[Foe.entity].find(foe => foe.name === FIXTURES.bandit.name);

    await encounter.setFoeCount(bandit, 4);

    ({ encounter } = wrapper.vm);
  });

  it('creates a run of the encounter', () => {
    expect(encounter.run).toBeInstanceOf(EncounterRun);
  });

  it('updates peer store with running encounter', () => {
    expect(store.state.peer.encounterRunId).toBe(encounter.run.id);
  });

  describe('encounter info', () => {
    it('displays the name', () => {
      expect(wrapper.find('.text-h4').text()).toBe(FIXTURES.brawl.name);
    });

    it('displays the difficulty and xp', () => {
      const text = wrapper.find('.text-h5').text();
      expect(text).toEqual(expect.stringContaining('hard'));
      expect(text).toEqual(expect.stringContaining('300 XP'));
    });

    it('displays tags', () => {
      expect(wrapper.find('.encounter__tags').text()).toBe('Tags: Tavern, Night');
    });
  });

  describe('combatants list', () => {
    let combatants;

    beforeEach(() => {
      combatants = wrapper.findAll('.encounter__combatant');
    });

    it('includes all foes and present members', () => {
      expect(combatants.length).toBe(6);
    });

    it('puts the party at the top', () => {
      const wolfram = combatants.at(0);
      expect(wolfram.find('.encounter__combatant-name').text()).toBe(FIXTURES.wolfram.name);
    });

    it.each([[1, FIXTURES.spy], [2], [3], [4], [5]])(
      'orders foe %i by initMod',
      (index, fixture = FIXTURES.bandit) => {
        const foe = combatants.at(index);
        expect(foe.find('.encounter__combatant-name').text()).toBe(fixture.name);
      },
    );

    it.each([[0, 'green'], [1, 'amber']])('highlights %i with %s', (index, colorClass) => {
      const combatant = combatants.at(index);
      expect(combatant.classes()).toContain(colorClass);
    });

    it.each([[2], [3], [4], [5]])('does not highlight %i', (index) => {
      const combatant = combatants.at(index);
      expect(combatant.classes()).not.toContain('green');
      expect(combatant.classes()).not.toContain('amber');
    });
  });

  describe('when initiatives are rolled', () => {
    let combatants;

    beforeEach(async () => {
      const wolfram = Member.query().where('name', FIXTURES.wolfram.name).first();
      wolfram.initiative = 19;
      wolfram.$save();
      await wrapper.find('.encounter__roll-foe-init').trigger('click');
      combatants = wrapper.findAll('.encounter__combatant');
    });

    it('reorders by initiative roll', () => {
      let prevInit = 0;
      for (let i = combatants.length - 1; i >= 0; i -= 1) {
        const combatant = combatants.at(i);
        const name = combatant.find('.encounter__combatant-name').text();
        const initField = combatant.find('.initiative-field');
        const initiative = +(initField.find('input').element.value);

        expect(initiative).toBeGreaterThanOrEqual(prevInit);
        expect(initiative).toBeGreaterThan(1);
        if (name === FIXTURES.wolfram.name) {
          expect(initiative).toBe(19);
        }

        prevInit = initiative;
      }
    });

    it('rolls instances of the same foe as one group', () => {
      let prevInit = 0;
      let prevName;
      for (let i = 0; i < combatants.length; i += 1) {
        const combatant = combatants.at(i);
        const name = combatant.find('.encounter__combatant-name').text();
        const initField = combatant.find('.initiative-field');
        const initiative = +(initField.find('input').element.value);

        if (prevName && prevName === name) {
          expect(initiative).toBe(prevInit);
        }

        prevInit = initiative;
        prevName = name;
      }
    });

    describe('reset foes', () => {
      beforeEach(async () => {
        await wrapper.find('.encounter__reset-foes').trigger('click');
        combatants = wrapper.findAll('.encounter__combatant');
      });

      it('puts all foes at init 0', () => {
        for (let i = combatants.length - 1; i >= 0; i -= 1) {
          const combatant = combatants.at(i);
          const name = combatant.find('.encounter__combatant-name').text();
          const initField = combatant.find('.initiative-field');
          const initiative = +(initField.find('input').element.value);

          if (name === FIXTURES.wolfram.name) {
            expect(initiative).toBe(19);
          } else {
            expect(initiative).toBe(0);
          }
        }
      });
    });

    describe('reset all', () => {
      beforeEach(async () => {
        await wrapper.find('.encounter__reset-all').trigger('click');
        combatants = wrapper.findAll('.encounter__combatant');
      });

      it('puts all foes and members at init 0', () => {
        for (let i = combatants.length - 1; i >= 0; i -= 1) {
          const combatant = combatants.at(i);
          const initField = combatant.find('.initiative-field');
          const initiative = +(initField.find('input').element.value);

          expect(initiative).toBe(0);
        }
      });
    });
  });
});
