import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import cloneDeep from 'lodash.clonedeep';

import db from '@/config/database';
import Foe from '@/models/Foe';
import Foes from '@/views/Foes.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

const bandit = {
  name: 'Bandit',
  tags: [{ value: 'medium' }, { value: 'humanoid' }],
  cr: 1 / 8,
};

const bugbear = {
  name: 'Bugbear',
  tags: [{ value: 'medium' }, { value: 'goblinoid' }],
  cr: 1,
};

describe('Foes.vue', () => {
  let wrapper;
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep({
      plugins: [VuexORM.install(db)],
    }));

    const vuetify = new Vuetify();

    wrapper = mount(Foes, { localVue, store, vuetify });
  });

  describe('add fab', () => {
    let addFoeBtn;

    beforeEach(() => {
      addFoeBtn = wrapper.find('.foes .v-btn--fab');
    });

    it('adds a new foe on icon click', async () => {
      const { length } = Foe.all();
      await addFoeBtn.trigger('click');
      expect(Foe.all().length).toBe(length + 1);
    });
  });

  describe('foe panel', () => {
    let foe;
    let expansionPanel;

    beforeEach(async () => {
      const entities = await Foe.create({ data: bandit });
      [foe] = entities[Foe.entity];
      expansionPanel = wrapper.find('.v-expansion-panel');
      await expansionPanel.find('.v-expansion-panel-header').trigger('click');
      await Vue.nextTick();
      expect(expansionPanel.classes()).toContain('v-expansion-panel--active');
    });

    it('can update the name', () => {
      const editNameField = expansionPanel.find('.edit-foe .v-text-field');
      expect(foe.name).toBe('Bandit');
      editNameField.vm.$emit('change', 'Thug');
      foe = Foe.find(foe.id);
      expect(foe.name).toBe('Thug');
    });

    it('can duplicate a foe', async () => {
      const duplicateBtn = expansionPanel.find('.v-btn');
      expect(duplicateBtn.text()).toBe('Duplicate');
      const { length } = Foe.all();
      await duplicateBtn.trigger('click');
      expect(Foe.all().length).toBe(length + 1);
      const duplicatePanel = wrapper.findAll('.v-expansion-panel').at(1);
      expect(expansionPanel.find('.v-expansion-panel-header').text()).toBe('1/8 - Bandit');
      expect(duplicatePanel.find('.v-expansion-panel-header').text()).toBe('1/8 - Bandit (duplicate)');
    });

    it('can delete a foe', async () => {
      const deleteBtn = expansionPanel.findAll('.v-btn').at(1);
      expect(deleteBtn.text()).toBe('delete');
      const { length } = Foe.all();
      await deleteBtn.trigger('click');
      expect(Foe.all().length).toBe(length - 1);
    });
  });

  describe('foes list', () => {
    let foes;
    let panels;

    beforeEach(async () => {
      const entities = await Foe.create({ data: [bugbear, bandit] });
      foes = entities[Foe.entity];
      panels = wrapper.findAll('.v-expansion-panel');
    });

    it('has an expansion panel for each foe', () => {
      expect(panels.length).toBe(foes.length);
    });

    it('is ordered by cr', () => {
      const sortedFoes = [...foes].sort((a, b) => a.cr - b.cr);
      expect(sortedFoes).not.toEqual(foes);
      sortedFoes.forEach((foe, index) => {
        const header = panels.at(index).find('.v-expansion-panel-header');
        expect(header.text()).toBe(`${foe.crText} - ${foe.name}`);
      });
    });

    describe('search', () => {
      let searchInput;
      let filteredPanels;

      beforeEach(() => {
        searchInput = wrapper.find('.v-text-field--solo input');
      });

      describe('by partial name', () => {
        beforeEach(async () => {
          searchInput.element.value = 'bugbe';
          await searchInput.trigger('input');
          filteredPanels = wrapper.findAll('.v-expansion-panel');
        });

        it('should filter to matching foes', () => {
          expect(wrapper.vm.search).toBe('bugbe');
          expect(filteredPanels.length).not.toBe(panels.length);
          expect(filteredPanels.length).toBe(1);
          expect(filteredPanels.at(0).text()).toBe('1 - Bugbear');
        });
      });

      describe('by cr', () => {
        beforeEach(async () => {
          searchInput.element.value = '1/8';
          await searchInput.trigger('input');
          filteredPanels = wrapper.findAll('.v-expansion-panel');
        });

        it('should filter to matching foes', () => {
          expect(wrapper.vm.search).toBe('1/8');
          expect(filteredPanels.length).not.toBe(panels.length);
          expect(filteredPanels.length).toBe(1);
          expect(filteredPanels.at(0).text()).toBe('1/8 - Bandit');
        });
      });

      describe('by tag', () => {
        beforeEach(async () => {
          searchInput.element.value = 'goblinoid';
          await searchInput.trigger('input');
          filteredPanels = wrapper.findAll('.v-expansion-panel');
        });

        it('should filter to matching foes', () => {
          expect(wrapper.vm.search).toBe('goblinoid');
          expect(filteredPanels.length).not.toBe(panels.length);
          expect(filteredPanels.length).toBe(1);
          expect(filteredPanels.at(0).text()).toBe('1 - Bugbear');
        });
      });
    });
  });
});
