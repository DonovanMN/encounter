import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import cloneDeep from 'lodash.clonedeep';

import initiative from '@/store/modules/initiative';

import Initiative from '@/views/Initiative.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

const MEMBERS = [{
  id: 6,
  initMod: 1,
  name: 'Wolfram',
  level: 4,
  present: true,
  initiative: 19,
  notes: '',
}];

const FOES = [{
  id: 5,
  encounter_run_id: 2,
  foe_id: 18,
  foe: {
    id: 18,
    initMod: 2,
    name: 'Mastiff',
    cr: 0.125,
    maxHitPoints: 5,
    armorClass: 12,
    tags: [],
    notes: '',
  },
  hitPoints: 5,
  initiative: 12,
  notes: '',
  hidden: true,
}];

describe('Initiative.vue', () => {
  let wrapper;
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep({
      modules: { initiative },
    }));

    const vuetify = new Vuetify();

    wrapper = mount(Initiative, {
      localVue,
      store,
      vuetify,
    });
  });

  describe('before connecting', () => {
    it('displays a message', () => {
      expect(wrapper.find('.text-subtitle-1').text()).toBe('Attempting to connect to DM...');
    });
  });

  describe('after connecting', () => {
    let connection;

    beforeEach(() => {
      connection = {
        open: true,
        send: jest.fn(),
      };
      store.commit('initiative/setConnection', connection);
    });

    it('waits to recieve the list of combatants', () => {
      expect(wrapper.find('.text-subtitle-1').text()).toBe('Waiting for DM to start an encounter...');
    });

    describe('with combatants', () => {
      const combatants = cloneDeep([...MEMBERS, ...FOES]);

      beforeEach(() => {
        store.commit('initiative/updateCombatants', combatants);
      });

      it('displays the combatants', () => {
        expect(wrapper.findAll('.initiative__combatant').length).toBe(combatants.length);
      });

      combatants.forEach((combatant, index) => {
        it(`displays combatant ${index + 1}`, () => {
          const nameEl = wrapper.findAll('.initiative__combatant-name').at(index);
          expect(nameEl.text()).toBe(combatant.hidden ? 'Hidden by DM' : combatant.name);
        });
      });

      describe('with turn', () => {
        beforeEach(() => {
          store.commit('initiative/updateTurn', { turn: 0, nextTurn: 1 });
        });

        it('highlights the current turn', () => {
          const classes = wrapper.findAll('.initiative__combatant').at(0).classes();
          expect(classes).toContain('green');
          expect(classes).toContain('accent-1');
        });

        it('highlights the next turn', () => {
          const classes = wrapper.findAll('.initiative__combatant').at(1).classes();
          expect(classes).toContain('amber');
          expect(classes).toContain('lighten-4');
        });
      });
    });
  });
});
