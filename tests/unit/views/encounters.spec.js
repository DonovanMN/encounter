import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import cloneDeep from 'lodash.clonedeep';

import db from '@/config/database';
import Foe from '@/models/Foe';
import Encounter from '@/models/Encounter';
import Tag from '@/models/Tag';

import ENCOUNTER_FIXTURES from '#/fixtures/encounter.fixtures';
import FOE_FIXTURES from '#/fixtures/foe.fixtures';

import Encounters from '@/views/Encounters.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

const { ambush, brawl } = ENCOUNTER_FIXTURES;

describe('Encounters.vue', () => {
  let appWrapper;
  let wrapper;
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep({
      plugins: [VuexORM.install(db)],
    }));

    const vuetify = new Vuetify();

    const App = localVue.component('App', {
      components: { Encounters },
      template: '<div id="app"><v-app><encounters/></v-app></div>',
    });

    appWrapper = mount(App, {
      localVue,
      store,
      vuetify,
      stubs: ['router-link'],
    });

    wrapper = appWrapper.findComponent(Encounters);

    // Force setTimeout and rAF to be synchronous for menu/autocomplete animations
    jest.spyOn(window, 'setTimeout').mockImplementation(cb => cb());
    jest.spyOn(window, 'requestAnimationFrame').mockImplementation(cb => cb());
  });

  afterEach(() => {
    window.setTimeout.mockRestore();
    window.requestAnimationFrame.mockRestore();
  });

  describe('add fab', () => {
    let addEncounterBtn;

    beforeEach(() => {
      addEncounterBtn = wrapper.find('.encounters .v-btn--fab');
    });

    it('adds a new encounter on icon click', () => {
      expect(Encounter.all().length).toBe(0);
      addEncounterBtn.trigger('click');
      expect(Encounter.all().length).toBe(1);
    });
  });

  describe('encounter card', () => {
    let card;

    beforeEach(async () => {
      await Encounter.create({ data: ambush });
      card = wrapper.find('.encounters__encounter-card');
    });

    it('displays the name', () => {
      expect(card.find('.text-h6').text()).toBe(ambush.name);
    });

    describe('edit dialog', () => {
      let editBtn;
      let dialog;
      let dialogContent;

      beforeEach(() => {
        editBtn = card.find('.v-dialog__container + .v-btn');
        dialog = card.find('.v-dialog__container');
        dialogContent = wrapper.find('.v-dialog');
      });

      it('is closed', () => {
        expect(dialog.vm.isActive).toBe(false);
        expect(dialogContent.exists()).toBe(false);
      });

      it('has edit button', () => {
        expect(editBtn.isVisible()).toBe(true);
      });

      describe('when opened', () => {
        let editEncounter;

        beforeEach(async () => {
          await editBtn.trigger('click');
          dialogContent = wrapper.find('.v-dialog');
          editEncounter = dialogContent.find('.edit-encounter');
        });

        it('displays the edit form', () => {
          expect(dialogContent.find('.v-card__title').text()).toBe('Edit Encounter');
          expect(editEncounter.isVisible()).toBe(true);
        });

        it('can edit the encounter name', async () => {
          const nameInput = editEncounter.find('input');
          expect(nameInput.element.value).toBe(ambush.name);
          const newName = 'Bandit Ambush';
          nameInput.element.value = newName;
          nameInput.trigger('input');
          await nameInput.trigger('keydown.enter');
          expect(card.find('.text-h6').text()).toBe(newName);
        });

        it('can edit the notes', async () => {
          const notesTextarea = editEncounter.find('.v-textarea');
          const textarea = notesTextarea.find('textarea');
          const notes = 'Some notes about the encounter';
          expect(textarea.element.value).toBe('');

          notesTextarea.vm.onFocus();
          await Vue.nextTick();
          textarea.element.value = notes;
          await textarea.trigger('input');
          notesTextarea.vm.onBlur();
          await Vue.nextTick();
          expect(card.find('.v-card__text .text-truncate').text()).toBe(notes);
        });

        describe('tag combobox', () => {
          let tagCombobox;

          beforeEach(() => {
            tagCombobox = editEncounter.find('.edit-encounter__edit-tags');
            return Tag.insert({ data: Tag.normalize('night') });
          });

          it('displays existing tags', () => {
            const chips = tagCombobox.findAll('.v-chip');
            expect(chips.length).toBe(1);
            expect(chips.at(0).text()).toBe('Foggy');
          });

          it('can add an existing tag', async () => {
            await tagCombobox.find('.v-select__slot').trigger('click');

            const autocompleteMenu = appWrapper.find('.v-autocomplete__content.menuable__content__active');
            expect(autocompleteMenu.isVisible()).toBe(true);

            const options = autocompleteMenu.findAll('.v-list-item');
            expect(options.length).toBe(1);
            await options.at(0).trigger('click');
            expect(autocompleteMenu.isVisible()).toBe(false);

            const chips = tagCombobox.findAll('.v-chip');
            expect(chips.length).toBe(2);
          });

          it('can delete a tag', async () => {
            let chips = tagCombobox.findAll('.v-chip');
            expect(chips.length).toBe(1);
            await chips.at(0).find('.v-chip__close').trigger('click');
            chips = tagCombobox.findAll('.v-chip');
            expect(chips.length).toBe(0);
          });
        });

        describe('foe autocomplete', () => {
          let foeAutocomplete;

          beforeEach(() => {
            foeAutocomplete = editEncounter.find('.edit-encounter__add-foes');
            return Foe.create({ data: Object.values(FOE_FIXTURES) });
          });

          it('can add a foe', async () => {
            expect(editEncounter.findAll('.edit-encounter__foes .v-list-item').length).toBe(0);
            await foeAutocomplete.find('.v-select__slot').trigger('click');

            const autocompleteMenu = appWrapper.find('.v-autocomplete__content.menuable__content__active');
            expect(autocompleteMenu.isVisible()).toBe(true);

            let options = autocompleteMenu.findAll('.v-list-item');
            expect(options.length).toBe(Object.keys(FOE_FIXTURES).length);

            const foeInput = foeAutocomplete.find('input');
            foeInput.element.value = 'As';
            await foeInput.trigger('input');

            options = autocompleteMenu.findAll('.v-list-item');
            expect(options.length).toBe(1);
            await options.at(0).trigger('click');
            expect(autocompleteMenu.isVisible()).toBe(false);
            expect(editEncounter.vm.foe).toBeDefined();

            const appendIcon = foeAutocomplete.find('.v-input__append-outer .v-icon');
            await appendIcon.trigger('click');
            expect(editEncounter.vm.foe).toBeUndefined();
            expect(editEncounter.findAll('.edit-encounter__foes .v-list-item').length).toBe(1);
          });
        });
      });
    });

    describe('menu', () => {
      let menuBtn;
      let menu;

      beforeEach(() => {
        menuBtn = card.find('.v-card__title .v-btn');
        menu = card.find('.v-card__title .v-menu');
      });

      it('is closed', () => {
        const menuContent = appWrapper.find('.v-menu__content');
        expect(menu.vm.isActive).toBe(false);
        expect(menuContent.exists()).toBe(false);
      });

      it('has menu button', () => {
        expect(menuBtn.isVisible()).toBe(true);
      });

      describe('when opened', () => {
        let menuContent;

        beforeEach(async () => {
          await menuBtn.trigger('click');
          menuContent = appWrapper.find('.v-menu__content');
        });

        it('adds content as child of <v-app>', () => {
          expect(menu.vm.isActive).toBe(true);
          expect(menuContent.isVisible()).toBe(true);
        });

        it('can delete an encounter', async () => {
          expect(card.exists()).toBe(true);
          const deleteMenuItem = menuContent.find('.v-list-item');
          await deleteMenuItem.trigger('click');
          expect(card.exists()).toBe(false);
        });
      });
    });
  });

  describe('encounters list', () => {
    let encounters;
    let cards;

    beforeEach(async () => {
      const entities = await Encounter.create({ data: [ambush, brawl] });
      encounters = entities[Encounter.entity];
      cards = wrapper.findAll('.encounters__encounter-card');
    });

    it('has an card for each encounter', () => {
      expect(cards.length).toBe(encounters.length);
    });

    it('is ordered by order', () => {
      const sortedEncounters = [...encounters].sort((a, b) => a.order - b.order);
      expect(sortedEncounters).not.toEqual(encounters);
      sortedEncounters.forEach((encounter, index) => {
        const title = cards.at(index).find('.text-h6');
        expect(title.text()).toBe(`${encounter.name}`);
      });
    });

    describe('search', () => {
      let searchInput;
      let filteredCards;

      beforeEach(() => {
        searchInput = wrapper.find('.v-text-field--solo input');
      });

      describe('by partial name', () => {
        beforeEach(async () => {
          searchInput.element.value = 'ambu';
          await searchInput.trigger('input');
          filteredCards = wrapper.findAll('.encounters__encounter-card');
        });

        it('should filter to matching encounters', () => {
          expect(wrapper.vm.search).toBe('ambu');
          expect(filteredCards.length).not.toBe(cards.length);
          expect(filteredCards.length).toBe(1);
          expect(filteredCards.at(0).find('.text-h6').text()).toBe(ambush.name);
        });
      });

      describe('by tag', () => {
        beforeEach(async () => {
          searchInput.element.value = 'foggy';
          await searchInput.trigger('input');
          filteredCards = wrapper.findAll('.encounters__encounter-card');
        });

        it('should filter to matching encounters', () => {
          expect(wrapper.vm.search).toBe('foggy');
          expect(filteredCards.length).not.toBe(cards.length);
          expect(filteredCards.length).toBe(1);
          expect(filteredCards.at(0).find('.text-h6').text()).toBe(ambush.name);
        });
      });
    });
  });
});
