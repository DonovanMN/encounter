import localForage from 'localforage';
import Member from '@/models/Member';
import Tag from '@/models/Tag';
import Foe from '@/models/Foe';
import FoeTag from '@/models/FoeTag';
import Encounter from '@/models/Encounter';
import EncounterTag from '@/models/EncounterTag';
import EncounterFoe from '@/models/EncounterFoe';
import EncounterRun from '@/models/EncounterRun';
import EncounterRunFoe from '@/models/EncounterRunFoe';

import store, { db, persist } from '@/store/store';

describe('store', () => {
  describe('modules', () => {
    it('has state for each module', () => {
      const stateKeys = Object.keys(store.state);
      expect(stateKeys.length).toBe(4);
      expect(stateKeys).toEqual(expect.arrayContaining(['entities', 'reload', 'peer', 'initiative']));
    });
  });

  describe('db', () => {
    it('has all models registered', () => {
      expect(db.entities.length).toBe(9);
      expect(db.entities).toEqual(expect.arrayContaining([
        expect.objectContaining({ name: 'members', model: Member }),
        expect.objectContaining({ name: 'tags', model: Tag }),
        expect.objectContaining({ name: 'foes', model: Foe }),
        expect.objectContaining({ name: 'foeTag', model: FoeTag }),
        expect.objectContaining({ name: 'encounters', model: Encounter }),
        expect.objectContaining({ name: 'encounterTag', model: EncounterTag }),
        expect.objectContaining({ name: 'encounterFoes', model: EncounterFoe }),
        expect.objectContaining({ name: 'encounterRuns', model: EncounterRun }),
        expect.objectContaining({ name: 'encounterRunFoes', model: EncounterRunFoe }),
      ]));
    });
  });

  describe('persistence', () => {
    it('is configured to localForage', async () => {
      expect(persist.storage).toBe(localForage);
      expect(persist.asyncStorage).toBe(true);
      expect(persist.key).toBe('vuex');
      expect(await localForage.getItem('vuex')).toBeNull();
    });

    describe('when entities are created', () => {
      beforeEach(() => (
        Member.create({ data: { name: 'Wolfram' } })
      ));

      it('they are persisted', async () => {
        const persisted = await localForage.getItem('vuex');
        expect(persisted).toBeDefined();
        expect(persisted.peer).toEqual({});
        expect(Object.keys(persisted.entities)).toEqual(expect.arrayContaining([
          'members',
          'tags',
          'foes',
          'foeTag',
          'encounters',
          'encounterTag',
          'encounterFoes',
          'encounterRuns',
          'encounterRunFoes',
        ]));
        expect(persisted.entities.members.data['1']).toEqual({
          id: 1,
          name: 'Wolfram',
          level: 1,
          initMod: 0,
          initiative: 0,
          notes: '',
          present: true,
        });
      });
    });
  });
});
