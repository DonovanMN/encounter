import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import cloneDeep from 'lodash.clonedeep';
import Peer from 'peerjs';

import db from '@/config/database';
import Member from '@/models/Member';
import peer from '@/store/modules/peer';
import initiative from '@/store/modules/initiative';

import PeerConnection from '@/components/PeerConnection.vue';

import FOE_FIXTURES from '#/fixtures/foe.fixtures';
import MEMBER_FIXTURES from '#/fixtures/member.fixtures';

jest.mock('peerjs');
jest.useFakeTimers();

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

const { wolfram } = MEMBER_FIXTURES;
const { bandit } = FOE_FIXTURES;
const hostId = 'the-peer-id-that-the-host-uses';
const path = `#/initiative/${hostId}`;

describe('Encounters.vue', () => {
  let wrapper;
  let store;
  let status;
  let connectMethod;
  let reconnectMethod;
  let peerOnMethod;
  let peerCallbacks;
  let connection;
  let sendMethod;
  let closeMethod;
  let connectionOnMethod;
  let connectionCallbacks;
  let disconnected = true;
  let destroyed = false;
  let queryCommandSupported;

  beforeEach(() => {
    connectMethod = jest.fn();
    reconnectMethod = jest.fn();

    peerCallbacks = {};
    peerOnMethod = jest.fn((name, callback) => {
      peerCallbacks[name] = callback;
    });

    Peer.mockImplementation(() => ({
      connect: connectMethod,
      reconnect: reconnectMethod,
      on: peerOnMethod,
      get destroyed() {
        return destroyed;
      },
      get disconnected() {
        return disconnected;
      },
    }));

    sendMethod = jest.fn();
    closeMethod = jest.fn();

    connectionCallbacks = {};
    connectionOnMethod = jest.fn((name, callback) => {
      connectionCallbacks[name] = callback;
    });

    connection = {
      send: sendMethod,
      close: closeMethod,
      on: connectionOnMethod,
      get open() {
        return true;
      },
    };

    queryCommandSupported = jest.fn();
    document.queryCommandSupported = queryCommandSupported;

    store = new Vuex.Store(cloneDeep({
      modules: { peer, initiative },
      plugins: [VuexORM.install(db)],
    }));
  });

  afterEach(() => {
    Peer.mockClear();
  });

  describe('when hosting', () => {
    let routerResolve;

    beforeEach(() => {
      routerResolve = jest.fn(({ name, params: { hostId: id } }) => ({
        href: `#/${name}/${id}`,
      }));

      wrapper = mount(PeerConnection, {
        localVue,
        store,
        vuetify: new Vuetify(),
        propsData: {
          host: true,
        },
        mocks: {
          $router: {
            resolve: routerResolve,
          },
        },
        stubs: ['router-link'],
      });

      status = wrapper.find('.text-capitalize');

      jest.runOnlyPendingTimers();
    });

    it('is disconnected', () => {
      expect(status.text()).toBe('disconnected');
    });

    it('initializes PeerJS', () => {
      expect(Peer).toHaveBeenCalled();
      expect(peerOnMethod).toHaveBeenCalledTimes(4);
      expect(peerCallbacks).toEqual({
        open: expect.any(Function),
        error: expect.any(Function),
        disconnected: expect.any(Function),
        connection: expect.any(Function),
      });
      expect(connectionOnMethod).not.toHaveBeenCalled();
      expect(connectionCallbacks).toEqual({});
    });

    describe('on peer open', () => {
      beforeEach(() => {
        disconnected = false;
        peerCallbacks.open(hostId);
      });

      it('is awaiting', () => {
        expect(status.text()).toBe('awaiting');
      });

      it('displays the peerId', () => {
        const secondItemTitle = wrapper.findAll('.v-list-item__title').at(1);
        expect(secondItemTitle.text()).toContain(`http://localhost/${path}`);
      });

      describe('on connection', () => {
        beforeEach(() => {
          peerCallbacks.connection(connection);
        });

        it('is connected', () => {
          expect(status.text()).toBe('connected');
        });

        it('listens for connection events', () => {
          expect(connection.on).toHaveBeenCalledTimes(3);
          expect(connectionCallbacks).toEqual({
            open: expect.any(Function),
            close: expect.any(Function),
            data: expect.any(Function),
          });
        });

        describe('on connection open', () => {
          beforeEach(() => {
            connectionCallbacks.open();
          });

          it('sends the current initiative run state', () => {
            const { calls } = sendMethod.mock;
            expect(calls).toEqual([
              [expect.objectContaining({ type: 'combatants' })],
              [expect.objectContaining({ type: 'turn' })],
            ]);
          });

          describe('when member initiative data is recieved', () => {
            let wolframId;

            beforeEach(async () => {
              const entities = await Member.create({ data: wolfram });
              [{ id: wolframId }] = entities[Member.entity];
              connectionCallbacks.data({
                type: 'updateMemberInitiative',
                where: wolframId,
                initiative: 18,
              });
            });

            it('updates the initiative', () => {
              expect(Member.find(wolframId).initiative).toBe(18);
            });
          });
        });
      });

      describe('on disconnected', () => {
        beforeEach(() => {
          disconnected = true;
          peerCallbacks.disconnected();
        });

        it('is disconnected', () => {
          expect(status.text()).toBe('disconnected');
        });

        it('attempts to reconnect', () => {
          expect(reconnectMethod).toHaveBeenCalledTimes(1);
        });
      });

      describe('when peer is destroyed', () => {
        beforeEach(() => {
          destroyed = true;
        });

        describe('on disconnected', () => {
          beforeEach(() => {
            disconnected = true;
            peerCallbacks.disconnected();
          });

          it('is disconnected', () => {
            expect(status.text()).toBe('disconnected');
          });

          it('attempts to reinitialize', () => {
            expect(reconnectMethod).not.toHaveBeenCalled();
            expect(Peer).toHaveBeenCalledTimes(2);
          });
        });
      });
    });
  });

  describe('when not hosting', () => {
    beforeEach(() => {
      connectMethod.mockReturnValue(connection);

      wrapper = mount(PeerConnection, {
        localVue,
        store,
        vuetify: new Vuetify(),
        mocks: {
          $route: {
            params: {
              hostId,
            },
          },
        },
      });

      status = wrapper.find('.text-capitalize');

      jest.runOnlyPendingTimers();
    });

    it('is connecting', () => {
      expect(status.text()).toBe('connecting');
    });

    it('initializes PeerJS', () => {
      expect(Peer).toHaveBeenCalled();
      expect(peerOnMethod).toHaveBeenCalledTimes(3);
      expect(peerCallbacks).toEqual({
        open: expect.any(Function),
        error: expect.any(Function),
        disconnected: expect.any(Function),
      });
      expect(connection.on).toHaveBeenCalledTimes(4);
      expect(connectionCallbacks).toEqual({
        open: expect.any(Function),
        close: expect.any(Function),
        error: expect.any(Function),
        data: expect.any(Function),
      });
    });

    describe('on connection open', () => {
      beforeEach(() => {
        connectionCallbacks.open();
      });

      it('is connected', () => {
        expect(status.text()).toBe('connected');
      });

      describe('when combatants are recieved', () => {
        let combatants;

        beforeEach(() => {
          combatants = [wolfram, bandit];
          connectionCallbacks.data({ type: 'combatants', combatants });
        });

        it('updates the initiative store', () => {
          expect(store.state.initiative.combatants).toBe(combatants);
        });
      });

      describe('when turn and nextTurn are recieved', () => {
        beforeEach(() => {
          connectionCallbacks.data({ type: 'turn', turn: 0, nextTurn: 1 });
        });

        it('updates the initiative store', () => {
          expect(store.state.initiative.turn).toBe(0);
          expect(store.state.initiative.nextTurn).toBe(1);
        });
      });
    });
  });
});
