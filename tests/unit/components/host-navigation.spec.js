import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import VueRouter from 'vue-router';
import router from '@/config/router';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import VuexORM from '@vuex-orm/core';
import cloneDeep from 'lodash.clonedeep';

import db from '@/config/database';
import Encounter from '@/models/Encounter';
import EncounterRun from '@/models/EncounterRun';
import peer from '@/store/modules/peer';

import HostNavigation from '@/components/HostNavigation.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(VueRouter);
localVue.use(Vuex);
localVue.use(Vuetify);

describe('HostNavigation.vue', () => {
  let wrapper;
  let store;
  let links;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep({
      modules: { peer },
      plugins: [VuexORM.install(db)],
    }));

    const vuetify = new Vuetify();

    wrapper = mount(HostNavigation, {
      localVue,
      router,
      store,
      vuetify,
    });

    links = wrapper.findAll('a.v-list-item');
  });

  it('has a list of links', () => {
    expect(links.wrappers.map(link => link.find('.v-list-item__content').text())).toEqual([
      'Encounters',
      'Foes',
      'Party',
      'Import/Export',
    ]);
  });

  describe('when an encounter is running', () => {
    beforeEach(async () => {
      let entities = await Encounter.create({ data: { name: 'Ambush' } });
      const [encounter] = entities[Encounter.entity];
      entities = await EncounterRun.insert({
        data: {
          encounter_id: encounter.id,
        },
      });
      const [run] = entities[EncounterRun.entity];
      store.commit('peer/runEncounter', run.id);
      await Vue.nextTick();
      links = wrapper.findAll('a.v-list-item');
    });

    it('it is added as a link', () => {
      expect(links.wrappers.map(link => link.find('.v-list-item__content').text())).toEqual([
        'Ambush',
        'Encounters',
        'Foes',
        'Party',
        'Import/Export',
      ]);
    });
  });
});
