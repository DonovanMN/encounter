import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuex from 'vuex';
import Vuetify from 'vuetify';
import cloneDeep from 'lodash.clonedeep';

import reload from '@/store/modules/reload';

import ReloadPrompt from '@/components/ReloadPrompt.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuex);
localVue.use(Vuetify);

describe('ReloadPrompt.vue', () => {
  let wrapper;
  let store;

  beforeEach(() => {
    store = new Vuex.Store(cloneDeep({
      modules: { reload },
    }));

    const vuetify = new Vuetify();

    wrapper = mount(ReloadPrompt, {
      localVue,
      store,
      vuetify,
    });
  });

  it('does not display a snackbar', () => {
    expect(wrapper.find('.v-snack__wrapper').isVisible()).toBe(false);
  });

  describe('when service worker has update ready', () => {
    let registration;
    let reloadBtn;

    beforeEach(async () => {
      registration = {
        waiting: {
          postMessage: jest.fn(),
        },
      };

      store.dispatch('reload/prompt', registration);
      reloadBtn = wrapper.find('.v-btn');
    });

    it('displays a snackbar', () => {
      expect(wrapper.find('.v-snack__wrapper').isVisible()).toBe(true);
      expect(wrapper.find('.v-snack__content').text()).toContain('This site has updated.');
      expect(reloadBtn.text()).toContain('Reload');
      expect(registration.waiting.postMessage).not.toHaveBeenCalled();
    });

    describe('when reload button is clicked', () => {
      beforeEach(() => {
        reloadBtn.trigger('click');
      });

      it('hides the snackbar', () => {
        expect(wrapper.find('.v-snack__wrapper').isVisible()).toBe(false);
      });

      it('posts a message to the service worker to skip waiting', () => {
        expect(registration.waiting.postMessage).toHaveBeenCalledTimes(1);
        expect(registration.waiting.postMessage).toHaveBeenCalledWith('skipWaiting');
      });
    });
  });
});
