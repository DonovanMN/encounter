import { mount, createLocalVue } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';

import HitPointField from '@/components/HitPointField.vue';

Vue.use(Vuetify);

const localVue = createLocalVue();

localVue.use(Vuetify);

describe('HitPointField.vue', () => {
  let wrapper;
  let input;
  let suffix;

  beforeEach(() => {
    const vuetify = new Vuetify();

    wrapper = mount(HitPointField, {
      localVue,
      vuetify,
    });

    input = wrapper.find('input');
    suffix = wrapper.find('.v-text-field__suffix');
  });

  it('defaults value and max 0', () => {
    expect(input.element.value).toBe('0');
    expect(suffix.text()).toBe('/0');
  });

  describe('when value and max props are set', () => {
    beforeEach(() => {
      wrapper.setProps({
        value: 42,
        max: 44,
      });
    });

    it('displays the values', () => {
      expect(input.element.value).toBe('42');
      expect(suffix.text()).toBe('/44');
    });

    describe('subtracting HP', () => {
      beforeEach(() => {
        input.element.value = '-10';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the decreased HP', () => {
        expect(wrapper.emitted().change).toEqual([[32]]);
      });
    });

    describe('adding HP', () => {
      beforeEach(() => {
        input.element.value = '+10';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the increased HP', () => {
        expect(wrapper.emitted().change).toEqual([[52]]);
      });
    });

    describe('adding a decimal HP', () => {
      beforeEach(() => {
        input.element.value = '+1.5';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the increased HP floored', () => {
        expect(wrapper.emitted().change).toEqual([[43]]);
      });
    });

    describe('setting HP', () => {
      beforeEach(() => {
        input.element.value = '14';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the set HP', () => {
        expect(wrapper.emitted().change).toEqual([[14]]);
      });
    });

    describe('setting HP to decimal', () => {
      beforeEach(() => {
        input.element.value = '2.75';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the floored HP', () => {
        expect(wrapper.emitted().change).toEqual([[2]]);
      });
    });

    describe('clearing HP', () => {
      beforeEach(() => {
        input.element.value = '';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the max HP', () => {
        expect(wrapper.emitted().change).toEqual([[44]]);
      });
    });

    describe('entering non-numerical value', () => {
      beforeEach(() => {
        input.element.value = 'foo42';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the curent HP', () => {
        expect(wrapper.emitted().change).toEqual([[42]]);
      });
    });

    describe('entering a non-finite value', () => {
      beforeEach(() => {
        input.element.value = 'Infinity';
        input.trigger('input');
        input.trigger('keydown.enter');
      });

      it('emits change with the curent HP', () => {
        expect(wrapper.emitted().change).toEqual([[42]]);
      });
    });
  });
});
