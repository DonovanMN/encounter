import FOE_FIXTURES from './foe.fixtures';

export default {
  ambush: {
    id: 1,
    name: 'Assassin Ambush',
    order: 2,
    tags: [{ value: 'foggy' }],
  },
  brawl: {
    id: 2,
    name: 'Bar Brawl',
    order: 1,
    foes: [FOE_FIXTURES.bandit, FOE_FIXTURES.spy],
    tags: [{ value: 'tavern' }, { value: 'night' }],
  },
};
