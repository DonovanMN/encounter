export default {
  assassin: {
    name: 'Assassin',
    initMod: 1,
    cr: 8,
    maxHitPoints: 44,
    armorClass: 16,
    tags: [{ value: 'medium' }, { value: 'humanoid' }],
  },
  bandit: {
    name: 'Bandit',
    initMod: 1,
    cr: 1 / 8,
    maxHitPoints: 11,
    armorClass: 12,
    tags: [{ value: 'medium' }, { value: 'humanoid' }],
  },
  spy: {
    name: 'Spy',
    initMod: 2,
    cr: 1,
    maxHitPoints: 27,
    armorClass: 12,
    tags: [{ value: 'medium' }, { value: 'humanoid' }],
  },
};
