export default {
  wolfram: {
    name: 'Wolfram',
    initMod: 2,
    level: 5,
  },
  renauld: {
    name: 'Renauld Manydeeds',
    initMod: 1,
    level: 5,
    present: false,
  },
};
